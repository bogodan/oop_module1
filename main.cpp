#include <iostream>
#include <string>
#include <vector>
#include <limits>


int main() {
    std::string input;
    int min1 = std::numeric_limits<int>::max();
    int min2 = std::numeric_limits<int>::max();
    int counter = 0;
    int number;
    do {
        std::cin >> input;
        number = std::stoi(input);
        if ( (number <= min1) || (number <= min2) ){
            if ( (min1 >= min2)){
                min1 = number;
            } else{
                min2 = number;
            }
        }
        counter++;
    }
    while(number <= 100);
    if (counter >=2){
        std::cout << min1 << " " << min2 << std::endl;
    }
    else{
        std::cout << std::min(min1,min2) << std::endl;
    }
    return 0;
}

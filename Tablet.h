//
// Created by bogodan on 09.11.20.
//

#ifndef OOP_MODULE1_TABLET_H
#define OOP_MODULE1_TABLET_H

#include "IObject.h"

class Tablet: public IObject {
public:
    std::string getName() const noexcept override;
};


#endif //OOP_MODULE1_TABLET_H

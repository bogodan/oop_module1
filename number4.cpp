//
// Created by bogodan on 09.11.20.
//
#include <iostream>
#include <memory>

#include "IObject.h"
#include "Phone.h"
#include "Tablet.h"

int main(){
    std::unique_ptr<IObject> obj = std::make_unique<Phone>();
    std::cout << obj->getName() << std::endl;
    obj = std::make_unique<Tablet>();
    std::cout << obj->getName();
    return 0;
}
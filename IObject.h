//
// Created by bogodan on 09.11.20.
//

#ifndef OOP_MODULE1_IOBJECT_H
#define OOP_MODULE1_IOBJECT_H

#include <string>

class IObject {
public:
     virtual std::string getName() const noexcept = 0;

private:
    std::string name;
};


#endif //OOP_MODULE1_IOBJECT_H
